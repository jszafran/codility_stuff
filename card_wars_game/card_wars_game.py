def solution(A, B):

    card_deck = {
        'A': 14,
        'K': 13,
        'Q': 12,
        'J': 11,
        'T': 10,
        '9': 9,
        '8': 8,
        '7': 7,
        '6': 6,
        '5': 5,
        '4': 4,
        '3': 3,
        '2': 2
    }

    A_wins_count = 0
    for i in range(0,len(A)):
        if card_deck[A[i]] > card_deck[B[i]]:
            A_wins_count += 1            
    return A_wins_count

def main():
    # some testcases
    print(solution('A586QK','JJ653K'))
    print(solution('23A84Q','K2Q25J'))

if __name__ == '__main__':
    main()
